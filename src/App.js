import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { translation } from './I18n/i18n';
function App () {
  const lang = useSelector(state => state.languageReducer.language)
  const dispatch = useDispatch()
  return (
    <div className='App'>
      <div className='box_btn'>
        <button className='btn btn-primary' onClick={(() => dispatch({ type: 'fr' }))}>francais</button>
        <button className='btn btn-danger ml-10' onClick={(() => dispatch({ type: 'en' }))}>anglais</button>
        <button className='btn btn-success ml-10' onClick={(() => dispatch({ type: 'vt' }))}>Vietnamien</button>
      </div>
      <div className='titre'>
        <h1>{translation(lang, 'titre')}</h1>
      </div>
    </div>
  )
}

export default App
