const initialState = {
  language: 'fr'
}

function languageReducer (state = initialState, action) {
  switch (action.type) {
    case 'fr':
      return { language: state.language = 'fr' }
    case 'en':
      return { language: state.language = 'en' }
      case "vt":
          return { language: state.language = 'vt' }
    default:
      return state
  }
}

export default languageReducer
