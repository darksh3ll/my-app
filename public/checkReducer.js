const initialState = {
  check: true
}

function checkReducer (state = initialState, action) {
  switch (action.type) {
    case 'check':
      return { check: !state.check }
    default:
      return state
  }
}

export default checkReducer
